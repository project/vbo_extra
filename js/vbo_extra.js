/**
 * @file
 * Attaches behaviors for the module.
 */

(function ($, Drupal) {

  Drupal.behaviors.vbo_extra = {
    attach: function(context, settings) {
      // Fix trigger.
      var $elements = $(context).find('.views-field-views-bulk-operations-bulk-form input[type="checkbox"], .vbo-select-all').once('vbo-extra');
      if ($elements.length) {
        $elements.each(function () {
          var $element = $(this).closest('.form-item');
          $element.find('label').on('click', function () {
            $element.find('input[type="checkbox"]').trigger({
              type: 'mousedown',
              which: 1
            });
            return true;
          });
        })
      }
    }
  };

})(jQuery, Drupal);
