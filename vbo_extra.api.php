<?php

/**
 * @file
 * API documentation for hooks.
 */

/**
 * Alter a vbo field form.
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param \Drupal\views\Plugin\views\field\FieldHandlerInterface $views_field
 */
function hook_vbo_extra_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, \Drupal\views\Plugin\views\field\FieldHandlerInterface $views_field) {
  // Code.
}
