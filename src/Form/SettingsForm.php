<?php

namespace Drupal\vbo_extra\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vbo_extra_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vbo_extra.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vbo_extra.settings');

    $form['settings'] = [
      '#title' => t('Settings'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $form['settings']['select_all_by_filters'] = [
      '#type' => 'checkbox',
      '#title' => t('Select all by filters'),
      '#default_value' => $config->get('select_all_by_filters'),
      '#description' => $this->t('Change logic of the Select all checkbox.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('vbo_extra.settings')
      ->set('select_all_by_filters', $form_state->getValue('select_all_by_filters'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
