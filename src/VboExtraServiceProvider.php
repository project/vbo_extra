<?php

namespace Drupal\vbo_extra;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Overrides the vbo data service.
 */
class VboExtraServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('views_bulk_operations.data')
      ->setClass('Drupal\vbo_extra\VboExtraViewData')
      ->addArgument(new Reference('config.factory'));
  }

}
