<?php

namespace Drupal\vbo_extra;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\views_bulk_operations\Service\ViewsBulkOperationsViewData;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Gets Views data needed by VBO.
 */
class VboExtraViewData extends ViewsBulkOperationsViewData {

  /**
   * The vbo extra config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $config_factory = NULL) {
    parent::__construct($event_dispatcher);
    $this->config = $config_factory->get('vbo_extra.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalResults($clear_on_exposed = FALSE) {
    if ($this->config->get('select_all_by_filters')) {
      $clear_on_exposed = TRUE;
    }

    return parent::getTotalResults($clear_on_exposed);
  }

}
