<?php

namespace Drupal\vbo_extra;

use Drupal\Core\Entity\EntityInterface;
use Drupal\views\Views;

/**
 * Vbo extra trait for executing a custom action.
 */
trait VboExtraCustomActionTrait {

  /**
   * The view.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * Sets the view.
   *
   * @param \Drupal\views\ViewExecutable $view
   */
  protected function setView($view) {
    $this->view = $view;
  }

  /**
   * Executes a main process for an action.
   *
   * @param bool $select_all
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function executeAction($select_all) {
    $data = $this->getTempstore();

    $entity_ids = [];
    foreach ($data['list'] as $item) {
      $entity_ids[] = $item[3];
    }

    if ($select_all) {
      $current = 0;
      $max = ceil($data['total_results'] / $data['batch_size']);
      $exclude_mode = $data['exclude_mode'];
      $results = [];
      while ($current <= $max) {
        $entities = $this->getViewEntities($current, $data['batch_size']);
        if ($exclude_mode) {
          $entities = array_filter($entities, function ($entity) use ($entity_ids) {
            return !in_array($entity->id(), $entity_ids);
          });
        }
        $results = array_merge($results, $this->processEntityMultiple($entities));
        $current++;
      }
    }
    else {
      $entity_type = $this->view->getBaseEntityType()->id();
      $entities = \Drupal::entityTypeManager()
        ->getStorage($entity_type)
        ->loadMultiple($entity_ids);
      $results = $this->processEntityMultiple($entities);
    }

    return $results;
  }

  /**
   * Gets a list of entities from a view.
   *
   * @param int $page
   * @param int $size
   *
   * @return EntityInterface[]
   */
  protected function getViewEntities($page, $size) {
    $entities = [];

    foreach ($this->getViewRows($page, $size) as $row) {
      $entity = $row->_entity;
      $entities[] = $entity;
    }

    return $entities;
  }

  /**
   * Gets results rows from a view.
   *
   * @param int $page
   * @param int $size
   * @param array $entity_ids
   *
   * @return array
   */
  protected function getViewRows($page, $size, $entity_ids = []) {
    if ($entity_ids) {
      $display_id = $this->view->current_display;
      $this->view = Views::getView($this->view->id());
      $this->view->setDisplay($display_id);
      $this->view->setExposedInput(['_vbo_extra_override' => TRUE]);
    }
    $this->view->setItemsPerPage($size);
    $this->view->setCurrentPage($page);
    $this->view->build();

    $offset = $size * $page;
    if ($pager_offset = $this->view->pager->getOffset()) {
      $offset += $pager_offset;
    }
    $this->view->query->setLimit($size);
    $this->view->query->setOffset($offset);

    if ($entity_ids) {
      $base_field = $this->view->storage->get('base_field');
      $base_field_alias = $this->view->query->fields[$base_field]['table'] . '.' . $this->view->query->fields[$base_field]['alias'];
      $this->view->query->addWhere(0, $base_field_alias, $entity_ids, 'IN');
    }

    $this->view->query->build($this->view);
    $this->view->query->execute($this->view);
    $this->view->render();
    return $this->view->result;
  }

  /**
   * Processes action for entities of results.
   *
   * @param array $entities
   *
   * @return array
   */
  protected function processEntityMultiple(array $entities) {
    $rows = [];
    foreach ($entities as $entity) {
      $rows[] = $this->processEntity($entity);
    }
    return $rows;
  }

  /**
   * Gets a vbo data.
   */
  protected function getTempstore() {
    $view_id = $this->view->id();
    $display_id = $this->view->current_display;
    $store_id = 'views_bulk_operations_' . $view_id . '_' . $display_id;
    $uid = \Drupal::currentUser()->id();
    return \Drupal::service('tempstore.private')->get($store_id)->get($uid);
  }

  /**
   * Sets a vbo data.
   */
  protected function setTempstore($value) {
    $view_id = $this->view->id();
    $display_id = $this->view->current_display;
    $store_id = 'views_bulk_operations_' . $view_id . '_' . $display_id;
    $uid = \Drupal::currentUser()->id();
    \Drupal::service('tempstore.private')->get($store_id)->set($uid, $value);
  }

  /**
   * Deletes a vbo data.
   */
  protected function deleteTempstore() {
    $view_id = $this->view->id();
    $display_id = $this->view->current_display;
    $store_id = 'views_bulk_operations_' . $view_id . '_' . $display_id;
    $uid = \Drupal::currentUser()->id();
    \Drupal::service('tempstore.private')->get($store_id)->delete($uid);
  }

  /**
   * Processes action for an entity of results.
   *
   * @param mixed
   */
  abstract protected function processEntity(EntityInterface $entity);

}
