<?php

namespace Drupal\vbo_extra\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bulk_operations\Plugin\views\field\ViewsBulkOperationsBulkForm;

/**
 * Defines the Views Bulk Operations field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("vbo_extra_form")
 */
class VboExtraForm extends ViewsBulkOperationsBulkForm {

  /**
   * The dummy action key.
   */
  const DUMMY_ACTION = '_none';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['vbo_extra_selection_info'] = ['default' => 'default'];
    $options['vbo_extra_buttons_position'] = ['default' => 'default'];
    $options['vbo_extra_custom_form'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['vbo_extra_selection_info'] = [
      '#type' => 'select',
      '#title' => $this->t('Selection info'),
      '#options' => [
        'default' => $this->t('Default'),
        'enabled' => $this->t('Displayed always'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $this->options['vbo_extra_selection_info'],
    ];

    $form['vbo_extra_buttons_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Buttons position'),
      '#options' => [
        'default' => $this->t('Default'),
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
      '#default_value' => $this->options['vbo_extra_buttons_position'],
    ];

    $form['vbo_extra_custom_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow customize form'),
      '#default_value' => $this->options['vbo_extra_custom_form'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Nothing.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    parent::viewsForm($form, $form_state);

    // Selection info.
    if ($this->options['vbo_extra_selection_info'] == 'enabled') {
      if (!isset($form['header'][$this->options['id']]['multipage'])) {
        $form['header'][$this->options['id']]['multipage'] = $this->getMultiPageElement();
      }
    }
    elseif ($this->options['vbo_extra_selection_info'] == 'disabled') {
      unset($form['header'][$this->options['id']]['multipage']);
    }

    // Set a buttons position.
    switch ($this->options['vbo_extra_buttons_position']) {
      case 'top':
        unset($form['actions']);
        break;

      case 'bottom':
        unset($form['header'][$this->options['id']]['actions']);
        break;
    }

    // Enable customize form.
    if ($this->options['vbo_extra_custom_form']) {
      $action_options = $this->getBulkOptions();
      unset($action_options[self::DUMMY_ACTION]);

      // Disable a default submit if actions is not set.
      if (empty($action_options)) {
        unset($form['header'][$this->options['id']]['action']);
        unset($form['header'][$this->options['id']]['actions']['submit']);
        unset($form['actions']['submit']);
      }

      // Unset a dummy action.
      unset($form['actions'][self::DUMMY_ACTION]);
      if ($this->options['buttons']) {
        unset($form['header'][$this->options['id']]['actions'][self::DUMMY_ACTION]);
      }
      else {
        unset($form['header'][$this->options['id']]['action']['#options'][self::DUMMY_ACTION]);
      }

      // Modules can alter vbo form.
      $this->moduleHandler->alter('vbo_extra_form', $form, $form_state, $this);
    }

    // Add a library for fix a bug of checkbox click.
    $form['#attached']['library'][] = 'vbo_extra/vbo_extra';
  }

  /**
   * {@inheritdoc}
   */
  public function viewsFormValidate(&$form, FormStateInterface $form_state) {
    if ($this->options['vbo_extra_custom_form']) {
      // Allows validate custom actions of the form.
      if (!($action = $form_state->getValue('action'))) {
        $trigger = $form_state->getTriggeringElement();
        $action = end($trigger['#parents']);
        $form_state->setValue('action', $action);
      }

      if ($action && !isset($this->actions[$action])) {
        $this->actions[$action] = $this->t('None');
      }
    }

    parent::viewsFormValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('vbo_extra.settings');
    $clear_on_exposed = $this->options['clear_on_exposed'];
    if ($config->get('select_all_by_filters')) {
      $this->options['clear_on_exposed'] = TRUE;
    }

    parent::viewsFormSubmit($form, $form_state);
    $this->options['clear_on_exposed'] = $clear_on_exposed;
  }

  /**
   * Only saves a temporary variable. Used with custom actions.
   */
  public function liteFormSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->get('step') == 'views_form_views_form') {
      // Update exclude mode setting.
      if ($form_state->getValue('select_all') && !empty($this->tempStoreData['list'])) {
        $this->tempStoreData['exclude_mode'] = TRUE;
      }
      else {
        $this->tempStoreData['exclude_mode'] = FALSE;
      }

      $this->setTempstoreData($this->tempStoreData);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function updateTempstoreData() {
    if ($this->options['vbo_extra_custom_form']) {
      // Fix an exposed input for using custom actions.
      $exposed_input = $this->view->getExposedInput();
      $this->view->setExposedInput($this->view->exposed_raw_input);
      parent::updateTempstoreData();
      $this->view->setExposedInput($exposed_input);
    }
    else {
      parent::updateTempstoreData();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getBulkOptions() {
    $options = parent::getBulkOptions();
    // Adds a dummy action to pass form validation.
    if ($this->options['vbo_extra_custom_form'] && empty($options)) {
      $options[self::DUMMY_ACTION] = $this->t('None');
    }
    return $options;
  }

  /**
   * Returns the multipage element.
   */
  protected function getMultiPageElement() {
    $page_selected = [];
    $base_field = $this->view->storage->get('base_field');
    foreach ($this->view->result as $row_index => $row) {
      $entity = $this->getEntity($row);
      $bulk_form_key = self::calculateEntityBulkFormKey(
        $entity,
        $row->{$base_field}
      );

      $checked = isset($this->tempStoreData['list'][$bulk_form_key]);
      if (!empty($this->tempStoreData['exclude_mode'])) {
        $checked = !$checked;
      }

      if ($checked) {
        $page_selected[] = $bulk_form_key;
      }
    }

    $count = empty($this->tempStoreData['exclude_mode']) ? count($this->tempStoreData['list']) : $this->tempStoreData['total_results'] - count($this->tempStoreData['list']);
    $element = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Selected %count items in this view', [
        '%count' => $count,
      ]),
      '#attributes' => [
        // Add view_id and display_id to be available for
        // js multipage selector functionality.
        'data-view-id' => $this->tempStoreData['view_id'],
        'data-display-id' => $this->tempStoreData['display_id'],
        'class' => ['vbo-multipage-selector'],
        'name' => 'somename',
      ],
    ];

    // Display a list of items selected on other pages.
    if ($count > count($page_selected)) {
      $form_data = $this->tempStoreData;
      $form_data['list'] = [];
      $form_data['relationship_id'] = $this->options['relationship'];
      foreach ($this->tempStoreData['list'] as $bulk_form_key => $item) {
        if (!in_array($bulk_form_key, $page_selected)) {
          $form_data['list'][$bulk_form_key] = $item;
        }
      }
      $this->addListData($form_data);
      $element['list'] = $this->getListRenderable($form_data);
      $element['list']['#title'] = empty($this->tempStoreData['exclude_mode']) ? $this->t('Items selected on other pages:') : $this->t('Items excluded on other pages:');
      $element['list']['#empty'] = $this->t('No selection');
    }

    $element['clear'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear'),
      '#submit' => [[$this, 'clearSelection']],
      '#limit_validation_errors' => [],
    ];

    return $element;
  }

}
